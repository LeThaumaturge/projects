#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>

#include "Grid.h"

using namespace std;

Grid::Grid(unsigned int height, unsigned int width)
    : mHeight(height), mWidth(width)
{
    cout << "Instantiate Grid object.." << endl;

    this->mValues = new unsigned int*[this->mHeight];
    for (unsigned int i = 0 ; i < this->mHeight ; i++) {
        this->mValues[i] = new unsigned int[this->mWidth];
    }
}

Grid::~Grid()
{
    cout << "Deleting Grid object.." << endl;

    for(unsigned int i = 0 ; i < this->mHeight ; ++i) {
        delete [] this->mValues[i];
    }
    delete [] this->mValues;
}

Grid::Grid(const Grid& other)
{
    //copy ctor
}

void Grid::LoadGridFile(const string& filename)
{
    ifstream fileStream; // Stream class to read from file
    string line;
    fileStream.open(filename);
    unsigned int counter = 0;

    if (fileStream.is_open()) {
        cout << "Loading Grid from file [" << filename << "].." << endl;

        while (getline(fileStream, line) && counter < this->mHeight) { // For each line
            istringstream iss(line);
//            cout << '\t';

            for (unsigned int i = 0 ; i < this->mWidth ; i++) {
                if (! (iss >> this->mValues[i][counter])) { break; }
//                cout << this->mValues[i][counter] << " ";
            }
            counter++;
//            cout << endl;
        }
        fileStream.close();
    } else {
        cout << "ERROR: " << strerror(errno) << " (" << filename << ")" <<  endl;
    }
}

void Grid::Print()
{
    unsigned int i = 0, j = 0;
    string separator = "\t+-----------------------+";

    cout << separator << endl;

    for (i = 0 ; i < this->mWidth ; i++) {
        if (i % 3 == 0)
            cout << "\t|";

        for (j = 0 ; j < this->mHeight ; j++) {

            this->mValues[j][i] == 0 ? cout << " ." : cout << " " << this->mValues[j][i];

            if ((j + 1) % 3 == 0)
                cout << " |";
        }
        if ((i + 1) % 3 == 0)
            cout << endl << separator << endl;
        else
            cout << endl << "\t|";
    }
}

Grid& Grid::operator=(const Grid& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}

ostream& operator<<(ostream& os, const Grid& grid)
{
    os << "GetHeight: " << grid.GetHeight() << endl;
    os << "GetWidth: " << grid.GetWidth() << endl;
    return os;
}
