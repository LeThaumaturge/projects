#include <iostream>

#include "Grid.h"

using namespace std;

int main()
{
    Grid* grid = new Grid(9, 9);
    grid->LoadGridFile("./grille.sdk");

    cout << "Printing loaded Grid to STDOUT.." << endl << endl;
    grid->Print();

    //cout << *grid;

    return 0;
}
