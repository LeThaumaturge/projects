#ifndef GRID_H
#define GRID_H

using namespace std;

class Grid
{
    public:
        Grid(unsigned int height, unsigned int width);
        ~Grid();
        Grid(const Grid& other);
        Grid& operator=(const Grid& other);
        friend ostream& operator<<(ostream& os, const Grid& other);

        unsigned int GetHeight() const { return mHeight; }
        void SetHeight(unsigned int val) { mHeight = val; }
        unsigned int GetWidth() const { return mWidth; }
        void SetWidth(unsigned int val) { mWidth = val; }
        void LoadGridFile(const string& filename);
        void Print();

    protected:

    private:
        unsigned int mHeight; // The actual height of the grid
        unsigned int mWidth; // The actual width of the grid
        unsigned int** mValues;  // The 2D array containing values
};

#endif // GRID_H
