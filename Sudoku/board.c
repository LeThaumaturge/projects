#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>
#include "board.h"

/** \brief
 *
 * \param height int La largeur du tableau
 * \param width int La hauteur du tableau
 * \return gameboard* Pointeur vers un objet gameboard initialisé
 *
 */
gameboard * gameboardInit(int height, int width)
{
    /**< Allocation mémoire de la structure de données */
    gameboard * gb = (gameboard *) malloc(sizeof (gameboard));
    if (gb == NULL) {
        fprintf(stderr, "gameboardInit()\t:\terror malloc !");
        exit(EXIT_FAILURE);
    }

    /**< Allocation mémoire */
    gb->position = (int **) malloc(sizeof (gb->position) * height);
    gb->height = height;
    gb->width = width;

    int i, j;
    /**< Allocation mémoire */
    for (i = 0 ; i < height ; i++) {
        gb->position[i] = (int *) malloc(sizeof (gb->position[i]) * width);

        /**< Initialisation à zéro */
        for (j = 0 ; j < width ; j++) {
            gb->position[i][j] = 0;
        }
    }
    return gb;
}

/** \brief
 *
 * \param file const char*
 * \param gb gameboard*
 * \return void
 *
 */
void gameboardLoad(const char * file, gameboard * gb)
{
    FILE *pfile = NULL;
    int number = 0, i = 0, j = 0;

    /**< Création du pointeur de fichier */
    if ((pfile = fopen(file, "r")) == NULL) {
        fprintf(stderr, "gameboardLoad()\t:\terror in fopen file \"%s\" !", file);
        exit(EXIT_FAILURE);
    }
    /**< Parcours du fichier et lecture des valeurs entières */
    while ( fscanf(pfile, "%d", &number) != EOF ) {
        gb->position[i][j++] = number;

        if (j == gb->width) {
            j = 0;
            i++;
        }
    }
    fclose(pfile);
}

/** \brief
 *
 * \param height int
 * \param width int
 * \param value int
 * \param gb const gameboard*
 * \return int
 *
 */
int gameboardIsValidPlacement(int height, int width, int value, const gameboard * gb) {
    int i, j;

    if (gb->position[height][width] != 0)
        return 0;

    /**< Vérification ligne horizontale */
    for (i = 0 ; i < gb->width ; i++) {
        if (gb->position[height][i] == value)
            return 0;
    }
    /**< Vérification ligne verticale */
    for (i = 0 ; i < gb->height ; i++) {
       if (gb->position[i][width] == value)
          return 0;
    }
    /**< Vérification section */
    int startH, startW;

    if (height < 3 && width < 3) {
        startH = 0;
        startW = 0;
    } else if (height < 3 && width < 6) {
        startH = 0;
        startW = 3;
    } else if (height < 3 && width < 9) {
        startH = 0;
        startW = 6;
    } else if (height < 6 && width < 3) {
        startH = 3;
        startW = 0;
    } else if (height < 6 && width < 6) {
        startH = 3;
        startW = 3;
    } else if (height < 6 && width < 9) {
        startH = 3;
        startW = 6;
    } else if (height < 9 && width < 3) {
        startH = 6;
        startW = 0;
    } else if (height < 9 && width < 6) {
        startH = 6;
        startW = 3;
    } else if (height < 9 && width < 9) {
        startH = 6;
        startW = 6;
    }

    for (i = startH ; i < startH + 3 ; i++) {
        for (j = startW ; j < startW + 3 ; j++) {
            if (gb->position[i][j] == value)
                return 0;
        }
    }

    return 1;
}

/** \brief
 *
 * \param height int
 * \param width int
 * \param value int
 * \param gb const gameboard*
 * \return void
 *
*/
int gameboardResolve(int height, int width, gameboard * gb)
{
    int value;

    if (gameboardIsComplete(gb)) { sleep(2); }

    /**< Les cases vides sont ignorées */
    if (gb->position[height][width] != 0) {
        /**< Mise à jour */
        if (height < 8) {
            gameboardResolve(height + 1, width, gb);
        } else {
            gameboardResolve(0, width + 1, gb);
        }
    } else {
        for (value = 0 ; value < 10 ; value++) {
                /**< Nécessaire : bug ? */
                gb->position[height][width] = 0;

            if (gameboardIsValidPlacement(height, width, value, gb)) {
                gb->position[height][width] = value;
                //sleep(1);
      //          system("clear");
                gameboardDisplay(gb);

                /**< Mise à jour */
                if (height < 8) {
                    gameboardResolve(height + 1, width, gb);
                } else {
                    gameboardResolve(0, width + 1, gb);
                }
            }
        }
        /**< Retour à l'appelant */
        gb->position[height][width] = 0;
    //    sleep(1);
    //    system("clear");
        gameboardDisplay(gb);
    }
    return 0;
}

int gameboardIsComplete(const gameboard * gb) {
    int i, j;

    for (i = 0 ; i < 9 ; i++) {
        for (j = 0 ; j < 9 ; j++) {
            if (gb->position[i][j] == 0)
                return 0;
        }
    }
    return 1;
}

/** \brief Affiche le contenu de la grille de jeu
 *
 * \param gb const gameboard* La grille de jeu à afficher
 * \return void
 *
 */
void gameboardDisplay(const gameboard * gb)
{
    int i, j, k;

    printf("\t+----");
    for (k = 0 ; k < gb->width ; k++) {
        printf("---");
    }
    printf("----+\n");

    for (i = 0 ; i < gb->height ; i++) {
        printf("\t");
        for (j = 0 ; j < gb->width ; j++) {
            if (gb->position[i][j] == 0) {
                printf("| . ");
            } else {
                printf("| %d ", gb->position[i][j]);
            }
        }
        if ((i + 1) % 3 == 0 && i != (gb->height -1)) {
            printf("|\n\t+----");
            for (k = 0 ; k < gb->width ; k++) {
                printf("---");
            }
            printf("----+\n");
        } else {
            printf("|\n");
        }
    }
    printf("\t+----");

    for (k = 0 ; k < gb->width ; k++) {
        printf("---");
    }
    printf("----+\n");
}
