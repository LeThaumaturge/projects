#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "board.h"

#define HEIGHT 9
#define WIDTH 9

int main()
{
    gameboard * plateau;
    plateau = gameboardInit(HEIGHT, WIDTH);
    gameboardLoad("./grille.sdk", plateau);
    gameboardResolve(0, 0, plateau);

    return 0;
}
