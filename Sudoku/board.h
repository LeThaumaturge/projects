#ifndef BOARD_H_INCLUDED
#define BOARD_H_INCLUDED

typedef struct _gameboard {
    int ** position;
    int height;
    int width;
} gameboard;

extern gameboard * gameboardInit(int height, int width);
extern void gameboardLoad(const char * file, gameboard * gb);
extern int gameboardIsValidPlacement(int height, int width, int value, const gameboard * gb);
extern int gameboardResolve(int height, int width, gameboard * gb);
extern int gameboardIsComplete(const gameboard * gb);
extern void gameboardDisplay(const gameboard * gb);

#endif // BOARD_H_INCLUDED
